%  ----------------------------------------------------------------------------
% Dieses Dokument ist eine Kurzversion von:
%
% Copyright (c) 2016 by Burkhardt Renz. All rights reserved.
% Vorlage Abschlussarbeit THM (minimal)
% $Id: vorlage.tex 3811 2016-08-05 12:03:51Z br $
% 
% Der Dokumentstyle wurde von "srcbook" auf "article" geändert, 
% um den Einstieg in LaTex zu erleichtern. 
% Die Unterdateien wurden nicht per \input ins Dokument geholt, 
% sondern direkt eingefügt. Chapters wurden in Sections 
% umbenannt. Das Titelblatt wurde verändert.
% Die Beschreibung der labeling-Umgebung wurde entfernt. ----------------------------------------------------------------------------

\documentclass[11pt]{scrartcl}       % KOMA-Skript für Artikel
	
%% Präambel
\usepackage[english, ngerman]{babel} % deutsche typogr. Regeln + Trenntabelle
\usepackage[T1]{fontenc}             % interner TeX-Font-Codierung
\usepackage{lmodern}                 % Font Latin Modern
\usepackage[utf8]{inputenc}          % Font-Codierung der Eingabedatei
\usepackage[babel]{csquotes}         % Anführungszeichen
\usepackage{graphicx}                % Graphiken
\usepackage{booktabs}                % Tabellen schöner
\usepackage{listingsutf8}            % Listings mit Einstellungen
\lstset{basicstyle=\small\ttfamily,
	tabsize=2,
	basewidth={0.5em,0.45em},
	extendedchars=true}
\usepackage{amsmath}	               % Mathematik
\usepackage[pdftex]{hyperref}       
\hypersetup{
	bookmarksopen=true,
	bookmarksopenlevel=3,
	colorlinks,
	citecolor=blue,
	linkcolor=blue,
}
\usepackage{scrhack}								 % unterdrückt Fehlermeldung von listings

%% Nummerierungstiefen
\setcounter{tocdepth}{3}             % 3 Stufen im Inhaltsverzeichnis
\setcounter{secnumdepth}{3} 		     % 3 Stufen in Abschnittnummerierung

% ----------------------------------------------------------------------------
\begin{document}



%% Titelseite
	
	
\titlehead{
\includegraphics[width=0.9\textwidth]{img/mni-logo}
}
\subject{Beispiel}
\title{Ein erstes Latex-Dokument}
\subtitle{Kurs \glqq Wissenschaftliches Arbeiten in der Informatik\grqq}
\author{Burkhardt Renz \and MNI\thanks{FB MNI der Technischen Hochschule Mittelhessen, THM}}
\date{Sommer 2016}
\publishers{Betreut und herausgegeben von\\
 Prof. Dr. Donald E. Knuth \cite{knuth99}
und Prof. Dr. Leslie Lamport \cite{lamport94}}
\dedication{Für alle Teilnehmer des Kurses}
\maketitle
	


%% Zusammenfassung

\begin{quote}
	\vspace*{2cm}

	\begin{center}
		\textbf{\Large\sffamily Zusammenfassung}
	\end{center}

	Dieser Text beschreibt sich in einem gewissen Sinne selbst, nämlich
	wie die \LaTeX-Datei aussieht, aus der dieses Dokument erzeugt wird.
	
	

	Die \LaTeX-Datei basiert auf KOMA-Script von Markus Kohm. KOMA-Script
	verwendet europäische typografische Konventionen. In aller Regel
	werden in der Vorlage Standardeinstellungen von KOMA-Skript
	übernommen. Darüberhinaus wird versucht eine möglichst einfache
	Vorlage zu erstellen, die leicht an eigene Bedürfnisse angepasst
	werden kann -- ohne dass man tiefergehende \LaTeX-Kenntnisse braucht.
\end{quote}
\vspace*{2cm}



%% Verzeichnissse
\tableofcontents
\listoffigures
\listoftables
\lstlistoflistings

\newpage

\section{Was ist \LaTeX und wie ist eine \LaTeX-Datei aufgebaut?}

\subsection{Etwas Geschichte -- oder ein paar Geschichten}

Die ersten Bände von \emph{The Art of Computer Programming}
(\textsc{TAOCP}) von Donald Knuth wurden im Bleisatz gesetzt. In den
1970er Jahren aber starb der Bleisatz aus und wurde durch den Fotosatz
ersetzt. Knuth war mit den damaligen Fotosatz-Systemen sehr unzufrieden,
weil sie mathematische Formeln nicht gut darstellen konnten. Das brachte
ihn auf die Idee selbst ein Satzsystem zu entwickeln, das für
Texte der Mathematik und Informatik typografisch hochwertige Ergebnisse
erreichen sollte. Zunächst dachte er, ein solches Programm könne in
einem Jahr oder so erstellt werden. Es dauerte dann doch etwa
länger.\footnote{ Die Entwicklung von \TeX\ ist auch ein interessanter
Fall von Software-Engineering, siehe Donald E. Knuth \emph{The Errors of
\TeX} in: Tom de Marco und Timothy Lister (Hrsg.) \emph{Software
State-of-the-Art: Selected Papers} New York NY: Dorste Publishing House,
1990.}

Knuths Idee bestand darin, dass Text, Formeln und Layout gewissermaßen
programmiert werden. Das Satzsystem konsumiert ein solches
\enquote{Programm} und macht daraus ein Dokument --- heutzutage in der
Regel ein PDF-Dokument. Das System sollte natürlich erweiterbar und
anpassbar sein, weshalb die Sprache, die Knuth entwickelt hat, im Grunde
eine Makrosprache ist und es deshalb auch erlaubt eigene Befehle zu
schreiben. Knuth hat einen Satz an Makros entwickelt, den man
\textsc{Plain}\kern2pt\TeX\ nennt -- diese Makros sind sehr nahe am Kern
von \TeX, \emph{low level} sozusagen.

Leslie Lamport\footnote{ Leslie Lamport hat wichtige Beiträge zur
Theorie verteilter Systeme geleistet.} sagt zur Entstehung  von \LaTeX\
(\url{http://research.microsoft.com/en-us/um/people/lamport/pubs/pubs.html#latex}):
\begin{quote}
In the early 80s, I was planning to write the Great American Concurrency
Book.  I was a \TeX\ user, so I would need a set of macros.  I thought
that, with a little extra effort, I could make my macros usable by
others.  Don Knuth had begun issuing early releases of the current
version of \TeX, and I figured I could write what would become its
standard macro package.  That was the beginning of \LaTeX. 	\\
\dots\\
Meanwhile, I still haven't written the Great American Concurrency
Book.
\end{quote}

\LaTeX\ trennt im Grunde die Befehle, die den Inhalt und die Struktur
eines Texts betreffen von den Befehlen, die für die typografische
Gestaltung sorgen.  \LaTeX\ stelt eine Reihe von sogenannten
Dokumentklassen bereit, die das Layout von Artikeln, Berichten, Büchern
und Briefen bereits enthalten, so dass man sich beim Schreiben nicht
mehr darum kümmern muss.

Die Dokumentklassen von \LaTeX\ erzeugen eine Typografie, die an
amerikanischen Konventionen orientiert ist. \textsf{KOMA-Script} ist eine
Sammlung von Dokumentklassen, die sich an europäischer Typografie
orientieren. Frank Neukam entwickelte Anfang der 1990er Jahr
\textsf{Script}, das Markus Kohm zu \textsf{KOMA-Script}
weiterentwickelte \cite{koma16}. Wir verwenden für die Vorlage die
Dokumentklasse von \textsf{KOMA-Script} für das Setzen von Büchern.

\subsection{Der Aufbau einer \LaTeX-Datei}

Eine \LaTeX-Datei beginnt mit der Angabe der Dokumentklasse mitsamt
ihren Optionen. Darauf folgt die sogenannte Präambel, in der benötigte
Pakete angegeben, Einstellungen festgelegt und auch eigene Makros
definiert werden. Darauf folgt in der Umgebung \verb=document= der
eigentliche Text.

\subsubsection{Dokumentklasse}

Wir verwenden für die Abschlussarbeit die Dokumentklasse \verb=scrbook=
von \textsf{KOMA-Script}. Diese Dokumentklasse ist vorgesehen für den
Satz von Büchern und sorgt dafür, dass Kapitel immer auf einer rechten
(also vorderen) Seite beginnen.

Die Dokumentklasse hat in der Regel geeignete Voreinstellungen für eine
Abschlussarbeit. Deshalb ändern wir nur einige wenige der Optionen von
\verb=scrbook=. In \cite{partosch15} werden weitere Optionen erläutert.

\subsubsection{Präambel}

In der Präambel binden wir benötigte Pakete ein.  Pakete sind
vorgefertigte Sammlungen von Makros für \LaTeX. Es  gibt für nahezu jede
denkbare Aufgabe solche Pakete, die man im \TeX\ Catalogue
\url{http://texcatalogue.ctan.org} finden kann.

Unsere Vorlage bindet die wichtigsten und für eine Arbeit im Feld der
Informatik in der Regel benötigten Pakete ein. Auch hierzu findet man
weitere Informationen in \cite{partosch15}.

Darüberhinaus stehen Einstellungen, bei uns zur Nummerierungstiefe, in
der Präambel.

Eigene Makros kann man auch in der Präambel unterbringen.

\subsubsection{Die Umgebung \texttt{document}}

Nach der Präambel kommt die Umgebung \verb=document=. In \LaTeX\ nennt
man Blöcke, die durch \verb=\begin{umgebung}= und \verb=\end{umgebung}=
begrenzt sind \emph{Umgebung}. In der Regel werden zu Beginn der
Umgebung bestimmte Einstellungen ein- und am Ende der Umgebung wieder
ausgeschaltet.

Der eigentliche Text der Arbeit steht also in der Klammer

\begin{lstlisting}
\begin{document}

% hierher kommt der eigentliche Text

\end{document}
\end{lstlisting}


\newpage


\section{Elemente für die Gliederung}

% Dieser Abschnitt wurde gegenüber der Vorlage verändert, weil es im Article keine Chapters gibt.
 
Eine Abschlussarbeit besteht aus drei großen Teilen:

\begin{itemize}
	\item Dem Vorderteil (\verb=frontmatter=) mit
		\begin{itemize}
			\item der Titelseite,
			\item der eidesstattlichen Erklärung,
			\item der Zusammenfassung,
			\item dem Inhaltsverzeichnis und
			\item den Verzeichnissen von Abbildungen, Tabellen und Listings,
		\end{itemize}
	\item dem Hauptteil (\verb=mainmatter=) mit den Kapiteln der Arbeit
		und
	\item dem Anhang (\verb=backmatter=) mit Anhängen und dem
		Literaturverzeichnis	
\end{itemize}


Die Dokumentklasse \verb=scrbook= hat als Möglichkeiten der Gliederung
zunächst den Teil (\verb=part=). Er wird in diesem Dokument nicht
verwendet und meistens ist eine Bachelor- oder Masterarbeit nicht so
umfangreich, dass man sie in Teile unterteilen muss.

Die nächste Ebene ist das Kapital (\verb=chapter=), wie wie auf dieser
Seite den Anfang eines solchen sehen. Kapitel beginnen immer auf einer
rechten Seite.

Dann kommt der Abschnitt (\verb=section=) -- in einem solchen befinden wir
uns im Moment.

\subsection{Unterabschnitt}

Dies ist eine \verb=subsection=.

\subsubsection{Unterunterabschnitt}

Jetzt sind wir noch eine Ebene tiefer, in der \verb=subsubsection=. Eine
Gliederung sollte ausgewogen sein, auch in Bezug auf die
Gliederungstiefe. Deshalb sollte man eher nicht bis zum
Unterunterabschnitt gehen.

\paragraph{Absatz mit Überschrift}

Dies ist ein Absatz mit Überschrift \verb=paragraph=. Die Überschrift
wird im Dokument hervorgehoben, hat aber keine eigene Zeile. Typografen
nennen das auch einen \enquote{Spieß}.

\subparagraph{Unterabsatz}

Dies ist ein Unterabsatz \verb=subparagraph=, auch ein \enquote{Spieß}.

\subsection{Untergliederungen im Text}

\subsubsection{Aufzählungen}

Man kann nummerierte Aufzählungen mit der Umgebung \verb=enumerate=
erzeugen. Dies eignet sich für Aufzählungen, die eine Reihenfolge haben
und ist oft einer Aneinanderreihung im Text vorzuziehen, weil
übersichtlicher.

Eine Bachelorarbeit besteht aus

\begin{enumerate}
	\item einem ersten Kapitel
		\begin{enumerate}
			\item einem ersten Abschnitt darin,
			\item einem zweiten Abschnitt darin,
		\end{enumerate}
	\item einem zweiten Kapitel
	\item usw. usf.	
\end{enumerate}

Aufzählungen, die keine inhaltliche Reihenfolge haben, kann man mit der
Umgebung \verb=itemize= darstellen:

\begin{itemize}
	\item eine wichtige Aussage
	\item noch eine wichtige Aussage
		\begin{itemize}
			\item mit einer Ausprägung
			\item und noch einer Ausprägung	
		\end{itemize}
	\item \dots
\end{itemize}

\subsubsection{Stichwortlisten}

Ein Beispiel für die Umgebung \verb=description= habe ich aus der
Dokumentation von \textsf{KOMA-Script} übernommen:

\begin{description}
	\item[empty] ist der Seitenstil, bei dem Kopf- und Fußzeile vollständig 
		leer bleiben. 
	\item[plain] ist der Seitenstil, bei dem keinerlei Kolumnentitel verwendet wird. 
	\item[headings] ist der Seitenstil für automatische Kolumnentitel. 
	\item[myheadings] ist der Seitenstil für manuelle Kolumnentitel. 
\end{description}

\newpage

\section{Elemente im Text}

\subsection{Typografische Elemente}

Im laufenden Text kann man alles Mögliche machen. Gute Typografie geht
mit diesen Möglichkeiten sparsam um. Was in Texten der Informatik oft
vorkommt ist die \emph{Hervorhebung} mit dem Befehl \verb=\emph=. Man
verwendet kursive Schrift auch für fremdsprachige Worte im Text, etwa
\textit{digital typography}.
Für Schlüsselworte, Befehle der Kommandozeile u.ä. nimmt man gerne einen
passenden Zeichensatz, etwa \texttt{make vorlage.pdf}. 

Das Paket \verb=csquotes= sorgt dafür dass Anführungszeichen
typografisch korrekt verwendet werden. Im Deutschen werden die
\enquote{Gänsefüßchen} nämlich anders als die angelsächischen
\foreignquote{english}{quotation marks} gesetzt.

Mehr über das Setzen von Text findet man in \cite{lkurz15}.

\subsection{Referenzen und Links}

Verweise auf die Literatur macht man mit dem Befehl \verb=\cite=.
Braucht man Querverweise auf Abschnitte im Text oder Abbildungen etc.,
so versieht man das Verweisziel mit einem \verb=\label= und verweist
dann mit \verb=\ref= oder \verb=\pageref=. Mehr dazu in
\cite[S.50]{partosch15}.

Links auf Quellen im Internet werden durch den Befehl \verb=\url=
angegeben und dadurch in PDF \enquote{anklickbar}, etwa wie
\url{https://tug.org/mactex/src/WelcomeToMacTeX.pdf}.

\subsection{Abbildungen}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=.4\textwidth]{img/knuth.jpg}
	\caption{Donald Knuth}
\label{fig:knuth}
\end{figure}

Abbildungen bindet man mit \verb=\includegraphics= in einer Umgebung
\verb=figure= ein.
Man kann dann im Text auf die Abbildung \ref{fig:knuth} verweisen.
Man muss beachten, dass die Abbildung in einer sogenannten
\enquote{fließenden} Umgebung eingebunden wird, d.h. beim Setzen des
Dokuments bestimmt \TeX, an welcher Stelle genau im Dokument die
Abbildung erscheint.

Wie man an diesem Beispiel sieht, kann man beliebige Abbildungen etwa
\verb=jpg= wie in diesem Beispiel, aber auch \verb=pdf= einbinden. Man
kann also Abbildungen mit einem Graphikprogramm erstellen, als
\verb=pdf= speichern und dann einbinden.

\newcommand{\tikz}{Ti\emph{k}Z}

Es gibt aber auch \tikz\ (= \tikz\ ist kein Zeichenprogramm), das der
deutsche Informatiker Till Tantau entwickelt hat. Damit ist es möglich,
Abbildungen zu \enquote{programmieren}. Die Projektseite von \tikz\ ist
\url{https://sourceforge.net/projects/pgf/}. Interessant sind auch
die Beispiele auf \url{http://www.texample.net/tikz/examples/}.

\subsection{Tabellen}

Tabellen werden in einer \enquote{fließenden} Umgaben namens
\verb=table= eingegeben. Der eigentliche Inhalt der Tabelle kommt in die
Umgebung \verb=tabular=.

\begin{table}[!htb]
	\centering
	\caption{Die Geschichte von \TeX\ und \LaTeX}
	\begin{tabular}{ r p{13cm}}
		\toprule
		Jahr & Entwicklung\\
		\midrule
		1977 & Donald Knuth beginnt mit der Entwicklung von \TeX.\\
		1985 & \LaTeX\ (mächtige Makros, die die Verwendung von \TeX\
			vereinfachen, in dem sie Struktur und Layout trennen) wird von
			Leslie Lamport in der Version 2.09 freigegeben.\\
		1986 & Feier der Fertigstellung von \TeX\ im Computer Museum in
		Boston\\
		1986 & Leslie Lamport veröffentlicht \emph{\LaTeX: A Document
			Preparation System}.\\
		1993 & \LaTeXe\\
		2000 & pdf\TeX\ (entwickelt von Hàn Thé Thành)\\
			?  & Nach dem Tod von Donald Knuth bekommt \TeX\ die Versionsnummer
			$\pi$.\\
		\bottomrule
	\end{tabular}
	\label{tbl:geschichte}
\end{table}

Wie man am Beispiel der Tabelle  \ref{tbl:geschichte} sieht, kann die
Formatierung von Tabellen etwas \enquote{sperrig} sein. Gut, dass man
sich in einer der vielen Anleitungen dazu erkundigen kann, z.B. in der
\LaTeXe-Kurzbeschreibung \cite[S.23]{lkurz15}.

\subsection{Listings}

\verb=listings= ist ein Paket, das es erlaubt, Code-Beispiele in die
Abschlussarbeit zu setzen. Wie das Beispiel \ref{lst:datei} zeigt, kann
man auch externe Dateien einbinden und im Text verbatim einsetzen. In
diesem Fall kann der eingebundene Text auch in der Zeichenkodierung
\emph{utf8} sein, wohingegen dies bei direkt im Text geschriebenen
Codebeispielen nicht unterstützt wird.

\begin{lstlisting}[caption={Einbinden einer Quelldatei}\label{lst:datei}]
% Der Inhalt der Datei myclass.java wird verbatim hier eingefuegt
\lstinputlisting[caption={Eine Java-Klasse}\label{lst:java}]{myclass.java}
\end{lstlisting}

Für Listings gibt es viele Optionen des Layouts, insbesondere ist es
möglich die Programmiersprache des eingebundenen Code-Beispiels
anzugeben, was dazu führt, dass Schlüsselworte, Bezeichner und Kommentare
der Sprache im Layout hervorgehoben werden.

\subsection{Mathematische Formeln}

Mathematisches kann man in den laufenden Text einbauen, wie etwa bei
foilgender Definition:

Mit $k!$, der Fakultät einer natürlichen Zahl $k$ bezeichnet man das
Produkt $1 \cdot 2 \cdot \dotso \cdot k$.

Oft braucht man aber auch ganze Abschnitte im Mathematik-Modus, wie in
folgendem Beispiel:

\begin{center}
\fbox{
	\begin{minipage}{0.9\textwidth}

	Die sogenannte Collatz-Folge (nach Lothar Collatz, deutscher Mathematiker
	1910 - 1990) zu einer natürlichen Zahl $n$ wird folgendermaßen
	gebildet:

	\begin{align}
	  n_1 &= n \nonumber \\
		n_{i+1} &= \left\{ \begin{array}{l l}
	                   n_i/2      & \textrm{falls $n_i$ gerade}\\
										 3 n_i + 1  & \textrm{falls $n_i$ ungerade}
										 \end{array} \right. \nonumber
	\end{align}

	\medskip
										 
	Startet man etwa mit der Zahl $7$ erhält man

	\[7\ 22\ 11\ 34\ 17\ 52\ 26\ 13\ 40\ 20\ 10\ 5\ 16\ 8\ 4\ 2\ 1\ 4\ 2\ 1\ \dots\]

	Wie man sieht, geht die Folge schließlich in den Zyklus $1, 4, 2$
	über.  Die \emph{Collatz-Vermutung} besagt, dass dies für jeden
	Startwert $n$ der Fall ist, d.h.  jede Collatz-Folge erreicht
	irgendwann den Wert $1$.
	\end{minipage}}
\end{center}

Mehr über das Setzen von mathematischen Formeln steht in \cite[Kapitel 4]{lkurz15}.






\newpage


\section{Installation von \LaTeX}

\subsection{Windows}

\subsubsection{Installationsschritte}

\begin{enumerate}
	\item Downloadseite von MiKTeX \url{http://www.miktex.org/download}.

	\item Die Basisversion \textbf{Basic MiKTeX 2.9 Installer} herunterladen.

	\item Die Installationsdatei namens \verb=basic-miktex-2.9.xxxx.exe=  
		starten.
		
	\item Dem Installationswizard folgen (am einfachsten die
		vorgeschlagenen Werte für Verzeichnisse usw. übernehmen).

	\item	Einen Ordner für die eigenen Dokumente erstellen, z.B. in \emph{Eigene
Dokumente} oder auf \verb=D:\\=

\end{enumerate}

\subsubsection{Erste Schritte mit \TeX works}

\begin{enumerate}
	\item Im Startmenü oder den Apps das Programm \emph{TeXworks} suchen
		und starten. Optional: Zur späteren Bequemlichkeit das Programm an die
		Taskleiste anheften. 

		Es erscheint das Editierfenster auf der linken Hälfte des Bildschirms.

	\item Zum ersten Ausprobieren im Menü \emph{File} den Unterpunkt \emph{New 
	 	from Template} auswählen und in dem dann erscheinenden Dialog \emph{Basic 
		LaTeX documents} und \emph{article.tex}

	\item	Die Datei wird im Editorfenster geöffnet. Um daraus das PDF-Dokument
		zu erstellen drückt man auf den grünen Button links oben.

		Nun öffnet sich ein Dialog zum Abspeichern des Dokuments. Danach:

	\item	Die LaTeX-Datei wird übersetzt. Im linken Fenster unten sieht man die
		Meldungen über den Fortschritt dieses Vorgangs. (Beim ersten Mal
		dauert das relativ lange, weil diverse Pakete für LaTeX aus dem
		Internet heruntergeladen werden.)

	\item	Nach einiger Zeit erscheint das Ergebnis in einem neuen Fenster auf
		der rechten Seite des Bildschirms.

	\item	Jetzt kann es losgehen: links editieren, Layout mit dem grünen Button
		starten und rechts das Ergebnis überprüfen.
\end{enumerate}

Viel Spaß mit \LaTeX.

\subsection{Mac OSX}

\subsubsection{Installation von Mac\TeX}

\begin{enumerate}
	\item Das Package MacTeX.pkg erhältlich bei
		\url{http://www.tug.org/mactex/} herunterladen.
	\item Öffnet man das Paket mit Doppelklick, startet die Installation
		und sie wird schrittweise durchgeführt.	
\end{enumerate}

\subsubsection{Arbeiten mit TeXShop}

Nach der Installation hat man im Launchpad eine App namens TeXShop. Dies
ist ein Editor für \TeX\ und \LaTeX.

\subsection{Linux}

Die einfachste Variante besteht darin, eine komplette \TeX
Live-Installation durchzuführen mittels

\begin{lstlisting}
sudo apt-get install texlive-full
\end{lstlisting}

Dabei wird allerdings vieles installiert, das man voraussichtlich
niemals braucht. Andererseits muss man aber nicht wissen, was man
mindestens installieren muss, damit \LaTeX\ verwendet werden kann.

Auf Github findet man Skripte, die Installationen z.B. unter Ubuntu
steuern können. Beispiel:
\url{https://github.com/scottkosty/install-tl-ubuntu}. Ich habe aber
keine Ahnung, wie gut diese Skripte tatsächlich sind.

\newpage


\begin{thebibliography}{99}                    % max. 99 Einträge möglich

\bibitem{lkurz15}
	Marco Daniel, Patrick Gundlach, Walter Schmidt, Jörg Knappen, Hubert
	Partl und Irene Hyna
	\emph{\LaTeXe-Kurzbeschreibung},
	\url{http://mirror.unicorncloud.org/CTAN/info/lshort/german/l2kurz.pdf},
	2015.
\bibitem{knuth99}
	Donald E. Knuth
	\emph{The \TeX book},
  Reading, MA: Addison-Wesley,
	1986.
\bibitem{koma16}
	Markus Kohm
	\emph{Die Anleitung \textsf{KOMA-Script}},
	\url{http://www.komascript.de/~mkohm/scrguide.pdf}
	2016.
\bibitem{lamport94}
  Leslie Lamport
  \emph{\LaTeX: a document preparation system},
  2nd edition,
  Reading, MA: Addison-Wesley,
  1994.
\bibitem{partosch15}
	Günter Partosch
	\emph{Anforderungen an wissenschaftliche Abschlussarbeiten und wie sie
	mit \LaTeX\ gelöst werden können},
	\url{https://www.staff.uni-giessen.de/partosch/unterlagen/abschlussarbeit.pdf},
	2015.

\end{thebibliography}


\end{document}
% ----------------------------------------------------------------------------
