\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einführung und Aufbau}{2}{section.1}%
\contentsline {section}{\numberline {2}Bekannte Ergebnisse - Literaturteil}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Die Arbeit von Hinz und Kunz}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Die Arbeit von Schneewittchen und den Zwergen}{3}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Die grundlegende Idee}{3}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Probleme bei der Durchführung}{3}{subsubsection.2.2.2}%
\contentsline {section}{\numberline {3}Der eigene wissenschaftliche Beitrag}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Das Dokument}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Die Analyse}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Neue Erkenntnisse}{4}{subsection.3.3}%
\contentsline {paragraph}{\nonumberline Mathematische Formeln:}{4}{paragraph*.9}%
\contentsline {paragraph}{\nonumberline Beispiel:}{4}{paragraph*.11}%
\contentsline {section}{\numberline {4}Ausblick}{4}{section.4}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
