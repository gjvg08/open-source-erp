\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{2}{section.1}%
\contentsline {section}{\numberline {2}Softwareauswahlprozess}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Kriterien der richtigen Auswahl}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Details des Auswahlprozess}{3}{subsection.2.2}%
\contentsline {section}{\numberline {3}Open-Source-Software}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Was ist Open-Source?}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Vor- und Nachteile von Open-Source-Software}{4}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Rechtliche Grundlagen}{4}{subsection.3.3}%
\contentsline {section}{\numberline {4}ERP}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Was ist ERP?}{5}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Unternehmerische Ansprüche an ERP-Lösungen}{5}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Open-Source ERP-Lösungen}{6}{subsection.4.3}%
\contentsline {section}{\numberline {5}Fazit}{6}{section.5}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
