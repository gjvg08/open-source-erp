### Feedback Grünert an Schick

1. Die Arbeit beschreibt Details des Softwareauswahlprozess, das Konzept von CRM und wann für ein Unternehmen Individualsoftware im CRM-Bereich in Frage kommt.
2. Der Autor recherchierte theoretische Grundlagen zu den Themenbereichen Softwareauswahl, CRM sowie Standard- bzw. Individualsoftware. Er konstruiert daraus zuerst eine Aufbereitung des bekannten Wissens zu diesen Grundthemen und bietet sie nacheinander da. Danach verbindet er diese Grundlagen in einem größtenteils selbst verfassten Resümee. Die leicht zugängliche Reorganisation von bekanntem Wissen ist dabei genauso Teil der wissenschaftlichen Eigenleistung wie das selbst verfasste Fazit.
3. Inhalts- und Literaturverzeichnis sind vorhanden. Die Arbeit besteht inklusive Deckblatt aus sieben Seiten, 4.5 Seiten davon sind Fließtext. Ein einziger Rechtschreibfehler liegt vor. Das Format entspricht den Anforderungen, die Zitationsverweise ragen allerdings manchmal über die Zeilenlänge hinaus.
Laut den Vorgaben zu wissenschaftlichen Arbeiten ist das Deckblatt nicht zu nummerieren, dies ist hier jedoch der Fall.
4. Eine Zusammenfassung fiel dank der Einleitung und dem klar definierten Inhaltsverzeichnis sehr leicht. Die Einleitung entsprach einer Zusammenfassung.
5. Die Recherche wirkte ausführlich und klar von der Eigenleistung des Autors getrennt. Der rote Faden der Gliederung spiegelte sich im Text gut wieder.  
Die Ausführlichkeit der Beschreibung des generellen Softwareauswahlprozesses war leicht lesbar, verständlich und nachvollziehbar. Ein Kapitel mit einem wörtlichen Zitat zu beginnen stimmt den Leser gut auf den folgenden Absatz ein.  
In Kapitel 5 ist kein einziges Zitat vorhanden, an manchen Stellen lässt der Text aber Fremdeinfluss vermuten.
6. Im Literaturverzeichnis fehlt der Verlagsort. Es wird das Konzept (nachname vorname, nachname vorname, ...) für die Darstellung der Autoren genutzt. Zitiert wird im Text mit [autorkürzelErscheinungsjahr].
7. Es wurden keine Floskeln oder künstlich aufgeblähte Sätze verwendet. Die Arbeit las sich wie von selbst.
8. Bombenarbeit, Bachelorthesis kann kommen, 10/10 würde wieder lesen.
